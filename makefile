ASM=nasm
ASMFLAGS=-f elf64
LD=ld

build: finder clean

words.inc: colon.inc

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm lib.o dict.o words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

finder: main.o dict.o lib.o
	$(LD)  -o $@ $^

.PHONY: clean build

clean:
	rm *.o
	
