%define syscall_exit 60
%define syscall_read 0
%define syscall_write 1
%define stderr 2
%define stdout 1
%define stdin 0
%define EOT_symbol 0x4
%define space_symbol 0x20
%define newline_symbol 0xA
%define tab_symbol 0x9
%define zero_symbol "0"
%define nine_symbol "9"
%define minus_symbol "-"
%define null 0

section .text
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, syscall_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        mov rax, -1
        .count:
                inc rax
                cmp byte [rdi+rax], 0
                jne .count
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	mov rsi, rdi
	push rsi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, stdout
	mov rax, syscall_write
	syscall
    ret

print_error:
	mov rsi, rdi
	push rsi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, stderr
	mov rax, syscall_write
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, syscall_write
	mov rdi, stdout
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, newline_symbol
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Вторым аргументом принимает fd, если равен 2, то вывод в stderr, иначе stdout
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
   	mov rax, rdi
	mov r9, rsi
	mov rsi, 10
	mov r8, rsp
	dec rsp
	mov byte [rsp], null
	.loop:
		xor rdx, rdx
		div rsi
		mov rdi, zero_symbol
		add rdi, rdx
		dec rsp
		mov byte [rsp], dil
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push r8
	cmp r9, stderr
	je .print_err
	call print_string
	pop r8
	mov rsp, r8
	ret
.print_err:
	call print_error
	pop r8
	mov rsp, r8
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, minus_symbol
	call print_char
	pop rdi
	neg rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov rdx, -1
	.loop:
		inc rdx
		mov al, byte [rdi+rdx]
		cmp al, byte [rsi+rdx]
		jne .not_equal
		cmp al, null
		jne .loop
	mov rax, 1
	ret
	.not_equal:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, syscall_read
	mov rdi, stdin
	push rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	.loop_skip_space:
		push rdi
		push rsi
		call read_char
		pop rsi
		pop rdi
		cmp rax, space_symbol
		je .loop_skip_space
		cmp rax, tab_symbol
		je .loop_skip_space
		cmp rax, newline_symbol
		je .loop_skip_space
	xor rdx, rdx
	.loop:
		cmp rax, space_symbol
		je .end
		cmp rax, tab_symbol
		je .end
		cmp rax, EOT_symbol
		je .end
		cmp rax, newline_symbol
		je .end
		cmp rax, null
		je .end
		mov byte [rdi+rdx], al
		inc rdx
		cmp rdx, rsi
		jae .err
		push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi
		jmp .loop
	.end:
		cmp rdx, rsi
		jae .err
		mov byte [rdi+rdx],null
		mov rax, rdi
		ret
	.err:
		xor rax, rax
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor rsi, rsi
        xor r9, r9
        mov r8, 10
.loop:
        mov sil, [rdi+r9]
        cmp sil, null
        je .end
        cmp sil, zero_symbol
        jb .end
        cmp sil, nine_symbol
        ja .end
        mul r8
        sub sil, zero_symbol
        add rax, rsi
        inc r9
        jmp .loop
.end:
        mov rdx, r9
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rsi, rsi
	cmp byte [rdi], minus_symbol
	jne .positive
	mov rsi, 1
	inc rdi
	.positive:
	push rsi
	call parse_uint
	pop rsi
	cmp rdx, 0
	je .skip
	cmp rsi, 1
	jne .skip
	neg rax
	inc rdx
	.skip:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jae .err
	xor rcx, rcx
	.loop:
		mov al, byte [rdi+rcx]
		cmp al, null
		je .end
		mov byte[rsi+rcx], al
		inc rcx
		jmp .loop
	.end:
		mov byte[rsi+rcx], null
		inc rcx
		mov rax, rcx
		ret
	.err:
		xor rax, rax
		ret
